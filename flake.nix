{
  description = "Flake for my school webdesign classes";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
  };

  outputs = { self, nixpkgs, flake-utils }: 
    let 
      system = "x86_64-linux";
    in 
    with import nixpkgs {
      system = system;
    }; {
      packages.${system}.default = pkgs.stdenv.mkDerivation {
        name = "schulzeugs";
        src = self;
        installPhase = "mkdir -p $out/; cp -r ./public/* $out";
      };
      devShell = pkgs.mkShell {
        buildInputs =  [];
      };
    };
}
